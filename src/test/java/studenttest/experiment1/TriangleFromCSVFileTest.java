package studenttest.experiment1;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import studenttest.experiment1.Triangle;

class TriangleFromCSVFileTest {

	@ParameterizedTest
	@DisplayName("三角形一般边界值测试")
	@CsvFileSource(resources = "/三角形一般边界测试用例.csv",numLinesToSkip=1)
	void testNormalBoundary(Integer num,Integer a,Integer b,Integer c,String type) {
		assertEquals(Triangle.classify(a, b, c), type);
	}
	@ParameterizedTest
	@DisplayName("三角形最坏情况边界值测试")
	@CsvFileSource(resources = "/三角形最坏情况测试用例.csv",numLinesToSkip=1)
	void testWorstCaseBoundary(Integer num,Integer a,Integer b,Integer c,String type) {
		assertEquals(Triangle.classify(a, b, c), type);
	}
	@ParameterizedTest
	@DisplayName("三角形健壮边界值测试")
	@CsvFileSource(resources = "/三角形健壮测试用例.csv",numLinesToSkip=1)
	void testRobustBoundary(Integer num,Integer a,Integer b,Integer c,String type) {
		assertEquals(Triangle.classify(a, b, c), type);
	}
	@ParameterizedTest
	@DisplayName("三角形健壮性最坏情况测试")
	@CsvFileSource(resources = "/三角形健壮性最坏情况测试用例.csv",numLinesToSkip=1)
	void testRobustWorstCaseBoundary(Integer num,Integer a,Integer b,Integer c,String type) {
		assertEquals(Triangle.classify(a, b, c), type);
	}
	
	@ParameterizedTest
	@DisplayName("三角形弱一般等价类测试")
	@CsvFileSource(resources = "/三角形弱一般等价类测试用例.csv",numLinesToSkip=1)
	void testWeakNormalEquivalence(Integer num,Integer a,Integer b,Integer c,String type) {
		assertEquals(Triangle.classify(a, b, c), type);
	}
	
	

}
